#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <tchar.h>
#include "Shlwapi.h"

#define SUCCESS (0)
#define FAILURE (-1)

#define BUFFERSIZE (100)

void PrintFileContent(TCHAR* pszFilePath)
{
	HANDLE hFile;
	DWORD  dwBytesRead = 0;
	char   readBuffer[BUFFERSIZE * sizeof(TCHAR)] = { 0 };
	BOOL bRetVal;

	hFile = CreateFile(pszFilePath,               // file to open
		GENERIC_READ,          // open for reading
		FILE_SHARE_READ,       // share for reading
		NULL,                  // default security
		OPEN_EXISTING,         // existing file only
		FILE_ATTRIBUTE_NORMAL, // normal file
		NULL);                 // no attr. template

	if (hFile == INVALID_HANDLE_VALUE)
	{
		_tprintf(_T("could not open %s."), pszFilePath);
		return;
	}

	bRetVal = ReadFile(hFile, readBuffer, BUFFERSIZE * sizeof(TCHAR), &dwBytesRead, NULL);

	if (bRetVal)
	{
		_tprintf(_T("The contents of the file are: %s.\n"), readBuffer);
	}
	else
	{
		_tprintf(_T("failed to read from the file...\n"));
	}

	bRetVal = CloseHandle(hFile);
	if (!bRetVal)
	{
		_tprintf(_T("Failed closing handle.\n"));
	}

	return;
}

TCHAR* ReadLineFromUser(DWORD dwLineLen)
{
	TCHAR* pszLine;
	INT iReturnValue;

	pszLine = (TCHAR *)malloc((dwLineLen + 1) * sizeof(TCHAR)); // allocating memory on the heap of MAX_PATH + 1(for null terminator)
	if (pszLine == NULL)
	{
		_tprintf(_T("Error allocating memory.\n"));
		return NULL;
	}

	iReturnValue = _tscanf_s(_T(" %100[^\n]"), pszLine, dwLineLen + 1); // reads the path until a new line is pressed.
	if (iReturnValue == 0)
	{
		_tprintf(_T("A return value of 0 indicates no fields were assigned"));
		return NULL;
	}
	else if (iReturnValue == EOF)
	{
		_tprintf(_T("The return value is EOF for an error, or if the end-of-file character "));
		_tprintf(_T("or the end-of-string character is found in the first attempt to read a character."));
		return NULL;
	}

	return pszLine;
}

BOOL WriteToFile(HANDLE hFile)
{
	DWORD dwBytesWritten = 0;
	BOOL bErrorFlag = FALSE;
	TCHAR* pszLine;
	DWORD dwBytesToWrite;

	_tprintf(_T("Please enter a line to write to the file: "));

	pszLine = ReadLineFromUser(100);
	if (pszLine == NULL)
	{
		_tprintf(_T("ReadLineFromUser failed.\n"));
		return FALSE;
	}

	dwBytesToWrite = (DWORD)_tcslen(pszLine) * sizeof(TCHAR);


	bErrorFlag = WriteFile(
		hFile,           // open file handle
		pszLine,      // start of data to write
		dwBytesToWrite,  // number of bytes to write
		&dwBytesWritten, // number of bytes that were written
		NULL);            // no overlapped structure

	if (FALSE == bErrorFlag)
	{
		_tprintf(_T("Terminal failure: Unable to write to file.\n"));
	}
	else
	{
		if (dwBytesWritten != dwBytesToWrite)
		{
			// This is an error because a synchronous write that results in
			// success (WriteFile returns TRUE) should write all data as
			// requested. This would not necessarily be the case for
			// asynchronous writes.
			_tprintf(_T("Error: dwBytesWritten != dwBytesToWrite\n"));
		}
		else
		{
			// _tprintf(TEXT("Wrote %d bytes successfully.\n"), dwBytesWritten);
		}
	}

	return TRUE;
}

HANDLE CreateFileAtPath(TCHAR* pszFilePath)
{
	HANDLE hFile;
	TCHAR folder[MAX_PATH] = { 0 };
	TCHAR *end;


	// loops over the path and creates every sub directory if needed
	end = _tcschr(pszFilePath, _T('\\'));
	while (end != NULL)
	{
		_tcsncpy_s(folder, MAX_PATH, pszFilePath, end - pszFilePath + 1);
		CreateDirectory(folder, NULL);
		end = _tcschr(++end, _T('\\'));
	}


	// creates the file
	hFile = CreateFile(pszFilePath,                // name of the write
		GENERIC_WRITE,          // open for writing
		0,                      // do not share
		NULL,                   // default security
		CREATE_NEW,             // create new file only
		FILE_ATTRIBUTE_NORMAL,  // normal file
		NULL);                  // no attr. template

	return hFile;
}

BOOL AskYesOrNo(void)
{
	TCHAR cAnswer;
	_tprintf(_T("Press y/Y for yes or n/N for no: "));
	_tscanf_s(_T(" %c"), &cAnswer, 1);
	if (cAnswer == _T('y') || cAnswer == _T('Y'))
	{
		return TRUE;
	}
	else if (cAnswer == _T('n') || cAnswer == _T('N'))
	{
		return FALSE;
	}
	else {
		_tprintf(_T("Invalid chooice, i will consider this as no.\n"));
		return FALSE;
	}

}


// asks the user to enter a path for a file and returns a pointer to the string.
TCHAR* GetPathFromUser(void)
{
	TCHAR* pszPath;
	INT iReturnValue;

	pszPath = (TCHAR *)malloc((MAX_PATH + 1) * sizeof(TCHAR)); // allocating memory on the heap of MAX_PATH + 1(for null terminator)
	if (pszPath == NULL)
	{
		_tprintf(_T("Error allocating memory.\n"));
		return NULL;
	}

	_tprintf(_T("Please enter a path for your file:\n"));
	iReturnValue = _tscanf_s(_T(" %260[^\n]"), pszPath, MAX_PATH + 1); // reads the path until a new line is pressed.
	if (iReturnValue == 0)
	{
		_tprintf(_T("A return value of 0 indicates no fields were assigned"));
		return NULL;
	}
	else if (iReturnValue == EOF)
	{
		_tprintf(_T("The return value is EOF for an error, or if the end-of-file character "));
		_tprintf(_T("or the end-of-string character is found in the first attempt to read a character."));
		return NULL;
	}

	return pszPath;
}


TCHAR* GetNewFilePath(void)
{
	TCHAR* pszPath;
	INT iReturnValuePath, iReturnValueDelete;
	BOOL bFileExists = TRUE;

	while (bFileExists)
	{
		pszPath = GetPathFromUser();
		if (pszPath == NULL)
		{
			return NULL;
		}

		iReturnValuePath = PathFileExists(pszPath);
		if (iReturnValuePath == 1)
		{
			_tprintf(_T("This file exists, do you want to delete it?\n"));
			if (AskYesOrNo())
			{
				iReturnValueDelete = DeleteFile(pszPath);
				if (iReturnValueDelete)
				{
					bFileExists = FALSE;
				}
				else
				{
					_tprintf(_T("could not delete the file %s.\n"), pszPath);
					return NULL;
				}
			}
			else
			{
				_tprintf(_T("Ok, try again.\n"));
			}
		}
		else
		{
			bFileExists = FALSE;
		}
	}

	return pszPath;
}


int main(void)
{
	TCHAR* pszPath;
	HANDLE hFile;
	BOOL bRetVal;

	pszPath = GetNewFilePath();
	if (pszPath == NULL)
	{
		_tprintf(_T("GetNewFilePath failed, try again.\n"));
		return FAILURE;
	}

	hFile = CreateFileAtPath(pszPath);
	if (hFile == NULL)
	{
		_tprintf(_T("CreateFileAtPath failed, try again.\n"));
		return FAILURE;
	}

	bRetVal = WriteToFile(hFile);
	if (bRetVal == FALSE)
	{
		_tprintf(_T("WriteToFile failed, try again.\n"));
		return FAILURE;
	}

	bRetVal = CloseHandle(hFile);
	if (!bRetVal)
	{
		_tprintf(_T("Failed closing handle.\n"));
	}

	PrintFileContent(pszPath);

	return SUCCESS;
}